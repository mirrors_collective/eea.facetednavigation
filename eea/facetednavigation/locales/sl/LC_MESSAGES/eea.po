msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2022-09-13 12:29+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: en\n"
"Language-Name: English\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: DOMAIN\n"

#: ../../facetednavigation/widgets/date/widget.py:20
msgid "1 month ago"
msgstr "1 mesec nazaj"

#: ../../facetednavigation/widgets/date/widget.py:22
msgid "1 week ago"
msgstr "1 teden nazaj"

#: ../../facetednavigation/widgets/date/widget.py:17
msgid "1 year ago"
msgstr "Pred 1 letom"

#: ../../facetednavigation/widgets/date/widget.py:24
msgid "2 days ago"
msgstr "2 dni nazaj"

#: ../../facetednavigation/widgets/date/widget.py:21
msgid "2 weeks ago"
msgstr "2 tednov nazaj"

#: ../../facetednavigation/widgets/date/widget.py:16
msgid "2 years ago"
msgstr "2 leti nazaj"

#: ../../facetednavigation/widgets/date/widget.py:19
msgid "3 months ago"
msgstr "3 mesecev nazaj"

#: ../../facetednavigation/widgets/date/widget.py:23
msgid "5 days ago"
msgstr "5 dni nazaj"

#: ../../facetednavigation/widgets/date/widget.py:18
msgid "6 months ago"
msgstr "6 mesecev nazaj"

#: ../../facetednavigation/widgets/multiselect/interfaces.py:57
msgid "AJAX URL"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:53
msgid "Accessor"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:54
msgid "Accessor to extract data for faceted widget."
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:59
msgid "Add"
msgstr "Uvozi"

#: ../../facetednavigation/browser/template/configure.pt:15
msgid "Add new filter"
msgstr "Dodaj nov filter"

#: ../../facetednavigation/browser/template/configure.pt:16
msgid "Add widget"
msgstr "Dodaj widget"

#: ../../facetednavigation/widgets/debug/widget.pt:25
msgid "After query filters"
msgstr "Po poizvedbi filtri"

#: ../../facetednavigation/widgets/alphabetic/widget.pt:24
#: ../../facetednavigation/widgets/radio/widget.pt:43
#: ../../facetednavigation/widgets/select/widget.pt:36
msgid "All"
msgstr "Vsa"

#: ../../facetednavigation/widgets/multiselect/interfaces.py:44
msgid "Allow multiple selections"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:47
msgid "Allow to input only numerical digits"
msgstr ""

#: ../../facetednavigation/widgets/alphabetic/widget.py:24
msgid "Alphabetic"
msgstr "Črkovna"

#: ../../facetednavigation/widgets/interfaces.py:48
msgid "An interface describing schema to be used within z3c.form"
msgstr ""

#: ../../facetednavigation/vocabularies/simple.py:38
msgid "Apple"
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:88
msgid "Are you sure you want to delete ${widget} widget?"
msgstr "Ali ste prepričani, da želite izbrisati ${widget} widget?"

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:23
msgid "Autocomplete"
msgstr ""

#: ../../facetednavigation/vocabularies/section.py:17
msgid "Basic search"
msgstr ""

#: ../../facetednavigation/widgets/boolean/widget.py:17
msgid "Boolean"
msgstr ""

#: ../../facetednavigation/vocabularies/position.py:22
msgid "Bottom"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:39
msgid "Can select multiple values"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:38
msgid "Can select several elements"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/interfaces.py:52
#: ../../facetednavigation/widgets/multiselect/interfaces.py:68
#: ../../facetednavigation/widgets/radio/interfaces.py:24
msgid "Catalog"
msgstr "Katalog"

#: ../../facetednavigation/widgets/alphabetic/interfaces.py:27
#: ../../facetednavigation/widgets/autocomplete/interfaces.py:17
#: ../../facetednavigation/widgets/boolean/interfaces.py:16
msgid "Catalog index"
msgstr "Katalog indeks"

#: ../../facetednavigation/widgets/interfaces.py:85
msgid "Catalog index to be used"
msgstr ""

#: ../../facetednavigation/widgets/alphabetic/interfaces.py:28
#: ../../facetednavigation/widgets/autocomplete/interfaces.py:18
#: ../../facetednavigation/widgets/boolean/interfaces.py:17
msgid "Catalog index to use for search"
msgstr "Katalog indeks uporabiti za iskanje"

#: ../../facetednavigation/widgets/debug/widget.pt:19
msgid "Catalog query"
msgstr "Katalog poizvedba"

#: ../../facetednavigation/vocabularies/position.py:21
msgid "Center bottom"
msgstr ""

#: ../../facetednavigation/vocabularies/position.py:19
msgid "Center top"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/widget.py:18
msgid "Checkboxes"
msgstr "Potrditvena polja"

#: ../../facetednavigation/vocabularies/simple.py:37
msgid "Classic"
msgstr ""

#: ../../facetednavigation/widgets/multiselect/interfaces.py:77
msgid "Close on select"
msgstr ""

#: ../../facetednavigation/widgets/multiselect/interfaces.py:78
msgid "Close selection popup after each select"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:19
msgid "Cloud"
msgstr "Cloud"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:110
msgid "Cloud height"
msgstr "Cloud višina"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:111
msgid "Cloud height (px)"
msgstr "Cloud višina (px)"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:90
msgid "Cloud type"
msgstr "Cloud vrsta"

#: ../../facetednavigation/browser/app/exportimport.py:50
msgid "Configuration imported"
msgstr "Nastavitve uvozi"

#: ../../facetednavigation/settings/menu.py:66
msgid "Configure"
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:99
msgid "Content listing"
msgstr "Seznamu vsebine"

#: ../../facetednavigation/widgets/daterange/interfaces.py:23
msgid "Control the range of yearsdisplayed in the year drop-down: either relative to today's year (-nn:+nn), relative to the currently selected year (c-nn:c+nn), absolute (nnnn:nnnn), or combinations of these formats (nnnn:-nn)."
msgstr "Nadzor nad vrsto yearsdisplayed v letu spustnem: bodisi glede na leto današnji (-nn: + nn), ki glede na trenutno izbrano leto (C-nn: c + nn), ki absolutno (nnnn: nnnn) ali s kombinacijo od teh formatov (nnnn:-nn)."

#: ../../facetednavigation/widgets/interfaces.py:108
msgid "Count results"
msgstr "Računajo rezultate"

#: ../../facetednavigation/widgets/debug/widget.pt:30
msgid "Counters"
msgstr "Števci"

#: ../../facetednavigation/browser/template/configure.pt:115
msgid "Criterion details"
msgstr "Merilo podrobnosti"

#: ../../facetednavigation/widgets/interfaces.py:59
msgid "Criterion interface"
msgstr ""

#: ../../facetednavigation/widgets/debug/widget.pt:35
msgid "Current configuration"
msgstr "Sedanji položaj"

#: ../../facetednavigation/widgets/interfaces.py:126
msgid "Custom CSS"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:127
msgid "Custom CSS class for widget to display in view page"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:64
msgid "Cut long phrases to provided number of characters"
msgstr "Cut dolge stavke na ponujeno število znakov"

#: ../../facetednavigation/widgets/date/widget.py:47
msgid "Date"
msgstr "Datum"

#: ../../facetednavigation/widgets/daterange/widget.py:62
msgid "Date range"
msgstr "Časovno obdobje"

#: ../../facetednavigation/widgets/debug/widget.py:14
msgid "Debugger"
msgstr "Razhroščevalnik"

#: ../../facetednavigation/vocabularies/simple.py:36
msgid "Default"
msgstr ""

#: ../../facetednavigation/widgets/etag/interfaces.py:23
msgid "Default e-tag"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/interfaces.py:20
#: ../../facetednavigation/widgets/multiselect/interfaces.py:18
msgid "Default items (one per line)"
msgstr "Privzeta polja (enega v vsako vrstico)"

#: ../../facetednavigation/widgets/alphabetic/interfaces.py:22
msgid "Default letter to be selected"
msgstr "Privzeto pismo, ki bodo izbrane"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:27
#: ../../facetednavigation/widgets/multiselect/interfaces.py:25
msgid "Default operator"
msgstr "Operater"

#: ../../facetednavigation/widgets/interfaces.py:80
msgid "Default query"
msgstr ""

#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:38
msgid "Default results per page"
msgstr "Privzete rezultati na stran"

#: ../../facetednavigation/widgets/resultsfilter/interfaces.py:21
#: ../../facetednavigation/widgets/tal/interfaces.py:22
msgid "Default tal expression for query value"
msgstr "Privzeto tal izraz za poizvedbe vrednost"

#: ../../facetednavigation/widgets/alphabetic/interfaces.py:21
#: ../../facetednavigation/widgets/boolean/interfaces.py:23
#: ../../facetednavigation/widgets/checkbox/interfaces.py:19
msgid "Default value"
msgstr "Privzeta vrednost"

#: ../../facetednavigation/browser/template/configure.pt:173
msgid "Delete"
msgstr "Izbriši"

#: ../../facetednavigation/widgets/path/interfaces.py:34
msgid "Depth to search the path. 0=this level, -1=all subfolders recursive, and any other positive integer count the subfolder-levels to search."
msgstr "Globina za iskanje poti. 0 = ta raven, -1 = vse podmape rekurzivne ter vsako drugo pozitivno celo število štetje podmape-raven iskati."

#: ../../facetednavigation/interfaces.py:80
msgid "Disable diazo rules on ajax requests"
msgstr ""

#: ../../facetednavigation/profiles/default/actions.xml
msgid "Disable faceted navigation"
msgstr "Onemogoči raznoliko navigacijo"

#: ../../facetednavigation/profiles/default/actions.xml
msgid "Disable faceted search"
msgstr "Onemogoči raznoliko iskanje"

#: ../../facetednavigation/settings/menu.py:82
msgid "Disable left portlets"
msgstr "Onemogoči levo portletov"

#: ../../facetednavigation/settings/menu.py:95
msgid "Disable right portlets"
msgstr "Onemogoči prave portletov"

#: ../../facetednavigation/settings/menu.py:108
msgid "Disable smart facets hiding"
msgstr "Onemogoči pametne plati skriva"

#: ../../facetednavigation/widgets/interfaces.py:109
msgid "Display number of results near each option"
msgstr "Prikaže število rezultatov ob vsaki možnosti"

#: ../../facetednavigation/widgets/interfaces.py:98
msgid "Display widget in section"
msgstr "Prikazovanje widget na delu"

#: ../../facetednavigation/browser/template/configure.pt:46
msgid "Display widget in section."
msgstr "Prikazovanje widget na odseku."

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:45
msgid "Do not display the search button"
msgstr ""

#: ../../facetednavigation/profiles.zcml:19
msgid "EEA Faceted Navigation"
msgstr ""

#: ../../facetednavigation/profiles/default/actions.xml
msgid "Enable faceted navigation"
msgstr "Omogoči raznoliko navigacijo"

#: ../../facetednavigation/profiles/default/actions.xml
msgid "Enable faceted search"
msgstr "Omogoči raznoliko iskanje"

#: ../../facetednavigation/widgets/criteria/interfaces.py:15
msgid "Enable hide/show criteria"
msgstr "Omogoči skriti / prikazati merila"

#: ../../facetednavigation/settings/menu.py:80
msgid "Enable left portlets"
msgstr "Omogoči levo portletov"

#: ../../facetednavigation/settings/menu.py:93
msgid "Enable right portlets"
msgstr "Omogoči prave portletov"

#: ../../facetednavigation/settings/menu.py:106
msgid "Enable smart facets hiding"
msgstr "Omogočanje pametnih plati skriva"

#: ../../facetednavigation/widgets/etag/interfaces.py:15
msgid "Enabled (hidden)"
msgstr ""

#: ../../facetednavigation/widgets/range/widget.pt:39
#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:23
msgid "End"
msgstr "Konec"

#: ../../facetednavigation/widgets/daterange/widget.pt:39
msgid "End date"
msgstr "Končni datum"

#: ../../facetednavigation/browser/template/error.pt:10
msgid "Error type:"
msgstr "Napaka tipa:"

#: ../../facetednavigation/browser/template/error.pt:12
msgid "Error value:"
msgstr "Vrednost napake:"

#: ../../facetednavigation/browser/template/configure.pt:77
msgid "Export"
msgstr "Izvoz"

#: ../../facetednavigation/vocabularies/section.py:18
msgid "Extended search"
msgstr ""

#: ../../facetednavigation/profiles.zcml:19
msgid "Extension profile for EEA Faceted Navigation."
msgstr ""

#: ../../facetednavigation/settings/configure.zcml:22
msgid "Faceted global settings"
msgstr "Obrazi globalne nastavitve"

#: ../../facetednavigation/views/configure.zcml:23
msgid "Faceted items preview"
msgstr ""

#: ../../facetednavigation/views/example/configure.zcml:22
msgid "Faceted listing view"
msgstr ""

#: ../../facetednavigation/subtypes/configure.zcml:59
msgid "Faceted navigable"
msgstr ""

#: ../../facetednavigation/settings/menu.py:24
msgid "Faceted navigation"
msgstr ""

#: ../../facetednavigation/subtypes/configure.zcml:59
msgid "Faceted navigation can be activated on the instances of this content type."
msgstr ""

#: ../../facetednavigation/subtypes/subtyper.py:105
msgid "Faceted navigation disabled"
msgstr "Obrazi navigacija onemogočena"

#: ../../facetednavigation/subtypes/subtyper.py:95
msgid "Faceted navigation enabled"
msgstr "Obrazi navigacija omogočeno"

#: ../../facetednavigation/settings/menu.py:25
msgid "Faceted navigation settings"
msgstr ""

#: ../../facetednavigation/subtypes/subtyper.py:135
msgid "Faceted search disabled"
msgstr "Obrazi iskanje invalide"

#: ../../facetednavigation/subtypes/subtyper.py:128
msgid "Faceted search enabled"
msgstr "Obrazi iskanje omogočen"

#: ../../facetednavigation/views/example/configure.zcml:14
msgid "Faceted summary view"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:37
msgid "Factory"
msgstr "Tovarna"

#: ../../facetednavigation/browser/app/configure.py:64
msgid "Filter added"
msgstr "Filter dodano"

#: ../../facetednavigation/browser/app/configure.py:152
msgid "Filter deleted"
msgstr "Filtrirajte črta"

#: ../../facetednavigation/widgets/sorting/interfaces.py:15
msgid "Filter from vocabulary"
msgstr "Filtrirajte iz besednjaka"

#: ../../facetednavigation/widgets/criteria/widget.py:14
msgid "Filters"
msgstr "Filtri"

#: ../../facetednavigation/browser/app/configure.py:91
msgid "Filters deleted"
msgstr "Filtri črta"

#: ../../facetednavigation/widgets/interfaces.py:69
msgid "Friendly name"
msgstr "Prijazno ime"

#: ../../facetednavigation/widgets/date/widget.py:90
msgid "Future"
msgstr "Prihodnost"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:53
#: ../../facetednavigation/widgets/multiselect/interfaces.py:69
#: ../../facetednavigation/widgets/radio/interfaces.py:25
msgid "Get unique values from catalog as an alternative for vocabulary"
msgstr "Get enolične vrednosti iz kataloga kot alternativa za besednjaka"

#: ../../facetednavigation/vocabularies/simple.py:39
msgid "Green"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:104
msgid "Hidden"
msgstr "skrit"

#: ../../facetednavigation/widgets/radio/interfaces.py:33
#: ../../facetednavigation/widgets/select/interfaces.py:33
#: ../../facetednavigation/widgets/tagscloud/interfaces.py:50
msgid "Hide 'All' option"
msgstr ""

#: ../../facetednavigation/widgets/criteria/widget.pt:18
msgid "Hide filters"
msgstr "Skrij filtri"

#: ../../facetednavigation/widgets/interfaces.py:120
msgid "Hide items with zero results"
msgstr "Skrij postavke z nič rezultatov"

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:44
msgid "Hide search button"
msgstr ""

#: ../../facetednavigation/widgets/etag/interfaces.py:16
msgid "Hide this widget in order for e-tag to be used"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:104
msgid "Hide widget"
msgstr "Skrij widget"

#: ../../facetednavigation/widgets/radio/interfaces.py:34
#: ../../facetednavigation/widgets/select/interfaces.py:34
#: ../../facetednavigation/widgets/tagscloud/interfaces.py:51
msgid "If this checkbox is checked, hides the \"All\" option"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:30
#: ../../facetednavigation/widgets/text/interfaces.py:24
msgid "If this checkbox is checked, hides the choice to filter in all items or in current items only"
msgstr ""

#: ../../facetednavigation/widgets/text/interfaces.py:33
msgid "If this checkbox is checked, the system will automatically do a wildcard search by appending a '*' to the search term so searching for 'budget' will also return elements containing 'budgetary'."
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:74
msgid "Import"
msgstr "Uvozi"

#: ../../facetednavigation/browser/template/configure.pt:66
msgid "Import - Export configuration"
msgstr "Uvoz - izvoz konfiguracije"

#: ../../facetednavigation/browser/template/configure.pt:67
msgid "Import configuration"
msgstr "Uvoz nastavitev"

#: ../../facetednavigation/settings/menu.py:167
msgid "Inheriting configuration if is now enabled"
msgstr ""

#: ../../facetednavigation/settings/menu.py:170
msgid "Inheriting configuration is now disabled"
msgstr ""

#: ../../facetednavigation/widgets/resultsperpage/widget.pt:19
msgid "Invalid or empty range provided for this widget. This widget will be ignored in view mode."
msgstr ""

#: ../../facetednavigation/widgets/sorting/widget.pt:21
msgid "Invalid or empty vocabulary provided for this widget. Leave empty for default sorting criteria."
msgstr "Neveljavno ali prazna besednjak če za to widget. Pustite prazno za privzete meril razvrščanja."

#: ../../facetednavigation/widgets/checkbox/widget.pt:31
#: ../../facetednavigation/widgets/multiselect/widget.pt:38
#: ../../facetednavigation/widgets/radio/widget.pt:29
msgid "Invalid or empty vocabulary provided for this widget. This widget will be ignored in view mode."
msgstr ""

#: ../../facetednavigation/widgets/range/widget.pt:1
msgid "Invalid range"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:59
msgid "Label (end date)"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:28
msgid "Label (end range)"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:53
msgid "Label (start date)"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:22
msgid "Label (start range)"
msgstr ""

#: ../../facetednavigation/vocabularies/position.py:18
msgid "Left"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/widget.pt:1
#: ../../facetednavigation/widgets/radio/widget.pt:1
msgid "Less"
msgstr ""

#: ../../facetednavigation/browser/template/view.pt:22
msgid "Less filters"
msgstr "Manj filtri"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:37
#: ../../facetednavigation/widgets/multiselect/interfaces.py:35
msgid "Let the end-user choose to search with AND or OR between elements"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:17
msgid "List"
msgstr "navedba"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:63
msgid "Maximum characters"
msgstr "Največje znakov"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:83
msgid "Maximum color"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:104
msgid "Maximum font-size (px)"
msgstr "Največja font-size (px)"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:61
#: ../../facetednavigation/widgets/radio/interfaces.py:39
#: ../../facetednavigation/widgets/tagscloud/interfaces.py:56
msgid "Maximum items"
msgstr "Največje postavke"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:103
msgid "Maximum size"
msgstr "Največja velikost:"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:76
msgid "Minimum color"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:97
msgid "Minimum font-size (px)"
msgstr "Minimum font-size (px)"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:96
msgid "Minimum size"
msgstr "Minimalna velikost"

#: ../../facetednavigation/widgets/checkbox/widget.pt:1
#: ../../facetednavigation/widgets/radio/widget.pt:1
msgid "More"
msgstr ""

#: ../../facetednavigation/browser/template/view.pt:21
msgid "More filters"
msgstr "Več filtri"

#: ../../facetednavigation/browser/template/configure.pt:156
msgid "Move Down"
msgstr "Premakni navz&dol"

#: ../../facetednavigation/browser/template/configure.pt:147
msgid "Move Up"
msgstr "Premakni navz&gor"

#: ../../facetednavigation/widgets/multiselect/widget.py:18
msgid "Multi Select"
msgstr ""

#: ../../facetednavigation/widgets/multiselect/interfaces.py:43
msgid "Multiselect"
msgstr ""

#: ../../facetednavigation/widgets/path/interfaces.py:25
msgid "Navigation js-tree starting point (relative to plone site. ex: SITE/data-and-maps)"
msgstr "Navigacija js-drevo izhodišče (glede na mesto Plone ex. MESTO / podatkov-in zemljevidi)"

#: ../../facetednavigation/widgets/date/widget.py:30
msgid "Next 2 days"
msgstr "Naslednja 2 dni"

#: ../../facetednavigation/widgets/date/widget.py:33
msgid "Next 2 weeks"
msgstr "Naslednja 2 tedna"

#: ../../facetednavigation/widgets/date/widget.py:38
msgid "Next 2 years"
msgstr "Naslednji 2 leti"

#: ../../facetednavigation/widgets/date/widget.py:35
msgid "Next 3 months"
msgstr "Naslednje 3 mesece"

#: ../../facetednavigation/widgets/date/widget.py:31
msgid "Next 5 days"
msgstr "Naslednjih 5 dni"

#: ../../facetednavigation/widgets/date/widget.py:36
msgid "Next 6 months"
msgstr "Naslednjih 6 mesecev"

#: ../../facetednavigation/widgets/date/widget.py:34
msgid "Next month"
msgstr "Naslednji mesec"

#: ../../facetednavigation/widgets/date/widget.py:32
msgid "Next week"
msgstr "Naslednji teden!"

#: ../../facetednavigation/widgets/date/widget.py:37
msgid "Next year"
msgstr "Naslednje leto"

#: ../../facetednavigation/vocabularies/simple.py:20
msgid "No"
msgstr ""

#: ../../facetednavigation/browser/app/exportimport.py:85
msgid "No action provided"
msgstr "Noben ukrep pod pogojem,"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:62
#: ../../facetednavigation/widgets/radio/interfaces.py:40
#: ../../facetednavigation/widgets/tagscloud/interfaces.py:57
msgid "Number of items visible in widget"
msgstr "Število predmetov vidnih v widget"

#: ../../facetednavigation/widgets/range/interfaces.py:46
msgid "Numerical range"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/interfaces.py:36
#: ../../facetednavigation/widgets/multiselect/interfaces.py:34
msgid "Operator visible"
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:116
msgid "Order"
msgstr "Da"

#: ../../facetednavigation/widgets/date/widget.py:77
msgid "Past"
msgstr "pretekli"

#: ../../facetednavigation/widgets/path/widget.py:21
msgid "Path"
msgstr "Pot"

#: ../../facetednavigation/widgets/portlet/interfaces.py:16
msgid "Path to portlet macro"
msgstr "Pot do portletov makro"

#: ../../facetednavigation/widgets/interfaces.py:74
msgid "Placeholder"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:71
msgid "Placeholder (end date)"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:40
msgid "Placeholder (end range)"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:65
msgid "Placeholder (start date)"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:34
msgid "Placeholder (start range)"
msgstr ""

#: ../../facetednavigation/browser/app/exportimport.py:38
msgid "Please provide a valid xml file"
msgstr ""

#: ../../facetednavigation/widgets/portlet/widget.py:19
msgid "Plone portlet"
msgstr "Plone portletov"

#: ../../facetednavigation/widgets/portlet/interfaces.py:15
msgid "Portlet macro"
msgstr "Portlet makro"

#: ../../facetednavigation/settings/menu.py:143
msgid "Portlets left column is hidden now"
msgstr "Portletov levi stolpec je zdaj skrit"

#: ../../facetednavigation/settings/menu.py:140
msgid "Portlets left column is visible now"
msgstr "Portletov levi stolpec je viden zdaj"

#: ../../facetednavigation/settings/menu.py:152
msgid "Portlets right column is hidden now"
msgstr "Portletov desni stolpec je zdaj skrit"

#: ../../facetednavigation/settings/menu.py:149
msgid "Portlets right column is visible now"
msgstr "Portletov desni stolpec je viden zdaj"

#: ../../facetednavigation/browser/template/configure.pt:33
#: ../../facetednavigation/widgets/interfaces.py:90
msgid "Position"
msgstr "Položaj:"

#: ../../facetednavigation/browser/template/configure.pt:34
msgid "Position of the widget."
msgstr "Stališče widget."

#: ../../facetednavigation/widgets/multiselect/interfaces.py:58
msgid "Provide an URL to be used to get and filter items asynchronously e.g.: /search. Your endpoint should filter items by 'q' param and return a JSON like e.g.: {'items': [...]}"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:38
msgid "Python name of a factory which can create the implementation object.  This must identify an object in a module using the full dotted name."
msgstr "Python ime tovarne, ki lahko ustvarijo izvajanja predmeta. To mora določiti predmet v modulu s polno pikčasto ime."

#: ../../facetednavigation/widgets/radio/widget.py:18
msgid "Radio"
msgstr "Radio"

#: ../../facetednavigation/widgets/range/widget.py:20
msgid "Range"
msgstr "Območje"

#: ../../facetednavigation/widgets/sorting/widget.pt:29
msgid "Relevance"
msgstr ""

#: ../../facetednavigation/widgets/criteria/widget.pt:30
msgid "Remove all"
msgstr "odstrani vse"

#: ../../facetednavigation/widgets/criteria/widget.pt:27
msgid "Remove all filters"
msgstr "Odstranite vse filtre"

#: ../../facetednavigation/widgets/resultsfilter/interfaces.py:20
#: ../../facetednavigation/widgets/resultsfilter/widget.py:38
msgid "Results Filter"
msgstr ""

#: ../../facetednavigation/widgets/resultsperpage/widget.py:20
msgid "Results per page"
msgstr "Rezultati po strani"

#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:24
msgid "Results per page ending value"
msgstr "Rezultati po strani se je končal vrednosti"

#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:17
msgid "Results per page starting value"
msgstr "Rezultati po strani začetna vrednost"

#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:31
msgid "Results per page step"
msgstr "Rezultati po korak stran"

#: ../../facetednavigation/widgets/daterange/interfaces.py:38
msgid "Reuse date format and language used by Plone"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:39
msgid "Reuse the same date format and the the same language that Plone uses elsewhere. Otherwise, the format will be \"yy-mm-dd\" and the language \"English\". Note that this default format allows you to encode very old or big years (example : 0001 will not be converted to 1901). Other formats do not."
msgstr ""

#: ../../facetednavigation/widgets/sorting/widget.pt:50
msgid "Reverse"
msgstr "Povratne"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:68
#: ../../facetednavigation/widgets/multiselect/interfaces.py:83
#: ../../facetednavigation/widgets/radio/interfaces.py:46
msgid "Reverse options"
msgstr "Povratne možnosti"

#: ../../facetednavigation/vocabularies/position.py:20
msgid "Right"
msgstr ""

#: ../../facetednavigation/widgets/path/interfaces.py:24
msgid "Root folder"
msgstr "Root mapa"

#: ../../facetednavigation/browser/template/configure.pt:172
msgid "Save"
msgstr "Shrani"

#: ../../facetednavigation/widgets/interfaces.py:47
msgid "Schema interface"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/widget.pt:38
#: ../../facetednavigation/widgets/text/widget.pt:28
msgid "Search"
msgstr "Išči"

#: ../../facetednavigation/widgets/path/interfaces.py:33
msgid "Search Depth"
msgstr "Iskanje Globina"

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:29
#: ../../facetednavigation/widgets/text/interfaces.py:23
msgid "Search in all elements only"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/interfaces.py:28
#: ../../facetednavigation/widgets/multiselect/interfaces.py:26
msgid "Search with AND/OR between elements"
msgstr "Iskanje z in / ali med elementi"

#: ../../facetednavigation/browser/template/configure.pt:45
#: ../../facetednavigation/widgets/interfaces.py:97
msgid "Section"
msgstr "Razdelek 3"

#: ../../facetednavigation/widgets/select/widget.py:16
msgid "Select"
msgstr "Izberi"

#: ../../facetednavigation/browser/template/configure.pt:104
msgid "Select all items"
msgstr "Izberite vse predmete"

#: ../../facetednavigation/widgets/autocomplete/interfaces.py:24
msgid "Select the source of the autocomplete suggestions"
msgstr ""

#: ../../facetednavigation/widgets/criteria/widget.pt:21
msgid "Show filters"
msgstr "Prikaži filtre"

#: ../../facetednavigation/settings/menu.py:161
msgid "Smart facets hiding is now disabled"
msgstr "Smart plati skriva je zdaj onemogočen"

#: ../../facetednavigation/settings/menu.py:158
msgid "Smart facets hiding is now enabled"
msgstr "Smart plati skriva je zdaj omogočeno"

#: ../../facetednavigation/widgets/interfaces.py:114
msgid "Sort by countable"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/interfaces.py:69
#: ../../facetednavigation/widgets/multiselect/interfaces.py:84
#: ../../facetednavigation/widgets/radio/interfaces.py:47
msgid "Sort options reversed"
msgstr "Sort Options obrnil"

#: ../../facetednavigation/widgets/sorting/widget.py:17
msgid "Sorting"
msgstr "Razvrščanje"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:18
msgid "Sphere"
msgstr "Krogla"

#: ../../facetednavigation/widgets/range/widget.pt:29
#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:16
msgid "Start"
msgstr "Začetek"

#: ../../facetednavigation/widgets/daterange/widget.pt:30
msgid "Start date"
msgstr "Datum začetka"

#: ../../facetednavigation/widgets/resultsperpage/interfaces.py:30
msgid "Step"
msgstr "Korak"

#: ../../facetednavigation/widgets/tal/widget.py:35
msgid "TAL Expression"
msgstr "TAL Expression"

#: ../../facetednavigation/widgets/tagscloud/widget.py:21
msgid "Tags Cloud"
msgstr "Tags Cloud"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:84
msgid "Tagscloud max color"
msgstr ""

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:77
msgid "Tagscloud minimum color"
msgstr ""

#: ../../facetednavigation/widgets/tal/interfaces.py:21
msgid "Tal Expression"
msgstr "Tal Expression"

#: ../../facetednavigation/widgets/text/widget.py:16
msgid "Text field"
msgstr "Besedilo polje"

#: ../../facetednavigation/widgets/autocomplete/widget.py:25
msgid "Text field with suggestions"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:60
msgid "Text to be displayed as end date input label"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:72
msgid "Text to be displayed as end date input placeholder"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:29
msgid "Text to be displayed as end range input label"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:41
msgid "Text to be displayed as end range input placeholder"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:75
msgid "Text to be displayed as input placeholder"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:54
msgid "Text to be displayed as start date input label"
msgstr ""

#: ../../facetednavigation/widgets/daterange/interfaces.py:66
msgid "Text to be displayed as start date input placeholder"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:23
msgid "Text to be displayed as start range input label"
msgstr ""

#: ../../facetednavigation/widgets/range/interfaces.py:35
msgid "Text to be displayed as start range input placeholder"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:121
msgid "This option works only if 'count results' is enabled"
msgstr "Ta možnost deluje le, če je omogočena davčna izpostava štetje results '"

#: ../../facetednavigation/widgets/path/widget.pt:24
msgid "This widget will be ignored as provided root is unreachable or it has no folderish children."
msgstr "Ta pripomoček se bo prezrta, kot je določeno koren je nedosegljiv ali nima folderish otrok."

#: ../../facetednavigation/browser/template/error.pt:3
msgid "This widget will be ignored in view mode as an error occurred while trying to render it. For more details please check the server log file. You can also try to modify widget configuration in order to see what property is causing this problem."
msgstr "Ta pripomoček ne bodo upoštevani v načinu s pogledom, kot je prišlo do napake, medtem ko poskuša to postane. Za več informacij si oglejte datoteko strežnika dnevnik. Prav tako lahko poskusite spremeniti widget konfiguracijo, da bi videli, kaj premoženje povzroča ta problem."

#: ../../facetednavigation/widgets/interfaces.py:70
msgid "Title for widget to display in view page"
msgstr "Naslov za widget za prikaz v pogledu strani"

#: ../../facetednavigation/widgets/date/widget.py:84
msgid "Today"
msgstr "Danes"

#: ../../facetednavigation/widgets/date/widget.py:29
msgid "Tomorrow"
msgstr "..."

#: ../../facetednavigation/vocabularies/position.py:17
msgid "Top"
msgstr ""

#: ../../facetednavigation/browser/template/configure.pt:21
msgid "Type"
msgstr "Tip"

#: ../../facetednavigation/widgets/tagscloud/interfaces.py:91
msgid "Type of the cloud"
msgstr "Vrsta oblaku"

#: ../../facetednavigation/browser/template/configure.pt:22
msgid "Type of the widget."
msgstr "Vrsta widget."

#: ../../facetednavigation/widgets/daterange/interfaces.py:22
msgid "UI Calendar years range"
msgstr "UI Koledar let segajo"

#: ../../facetednavigation/widgets/criteria/interfaces.py:16
msgid "Uncheck this box if you don't want hide/show criteria feature enabled on this widget"
msgstr "Počistite to polje, če ne želite Skrij / prikaži merila funkcija omogočena ta widget"

#: ../../facetednavigation/profiles.zcml:28
msgid "Uninstall EEA Faceted Navigation"
msgstr ""

#: ../../facetednavigation/profiles.zcml:28
msgid "Uninstall to remove EEA Faceted Navigation support"
msgstr ""

#: ../../facetednavigation/widgets/interfaces.py:115
msgid "Use the results counter for sorting"
msgstr ""

#: ../../facetednavigation/widgets/debug/interfaces.py:15
msgid "Visible to"
msgstr "Vidni"

#: ../../facetednavigation/widgets/checkbox/interfaces.py:45
#: ../../facetednavigation/widgets/multiselect/interfaces.py:50
#: ../../facetednavigation/widgets/radio/interfaces.py:17
msgid "Vocabulary"
msgstr "Besednjak"

#: ../../facetednavigation/widgets/sorting/interfaces.py:16
msgid "Vocabulary to use to filter sorting criteria. Leave empty for default sorting criteria."
msgstr "Besednjak uporabljajo za filtriranje razvrščanje merila. Pustite prazno za privzete meril razvrščanja."

#: ../../facetednavigation/widgets/checkbox/interfaces.py:46
#: ../../facetednavigation/widgets/multiselect/interfaces.py:51
#: ../../facetednavigation/widgets/radio/interfaces.py:18
msgid "Vocabulary to use to render widget items"
msgstr "Besednjak za uporabo, da postane gradnik predmetov"

#: ../../facetednavigation/widgets/interfaces.py:91
msgid "Widget position in page"
msgstr "Widget položaj stran"

#: ../../facetednavigation/browser/template/configure.pt:139
msgid "Widget type"
msgstr "Widget vrsta"

#: ../../facetednavigation/widgets/debug/interfaces.py:16
msgid "Widget will be visible only for selected user"
msgstr "Gradnik bo viden le za izbranega uporabnika"

#: ../../facetednavigation/widgets/text/interfaces.py:32
msgid "Wildcard search"
msgstr ""

#: ../../facetednavigation/vocabularies/simple.py:21
msgid "Yes"
msgstr ""

#: ../../facetednavigation/widgets/date/widget.py:25
msgid "Yesterday"
msgstr "Včeraj"

#: ../../facetednavigation/widgets/checkbox/widget.pt:36
#: ../../facetednavigation/widgets/multiselect/widget.pt:65
msgid "all"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/widget.pt:49
#: ../../facetednavigation/widgets/text/widget.pt:39
msgid "all items"
msgstr "Vsi elementi"

#: ../../facetednavigation/widgets/checkbox/widget.pt:36
#: ../../facetednavigation/widgets/multiselect/widget.pt:65
msgid "any"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/widget.pt:58
#: ../../facetednavigation/widgets/text/widget.pt:48
msgid "in current results"
msgstr "V trenutnih rezultatih"

#: ../../facetednavigation/widgets/checkbox/widget.pt:35
#: ../../facetednavigation/widgets/multiselect/widget.pt:64
msgid "match"
msgstr ""

#: ../../facetednavigation/widgets/checkbox/widget.pt:34
#: ../../facetednavigation/widgets/multiselect/widget.pt:63
msgid "match any/all filters bellow"
msgstr ""

#: ../../facetednavigation/widgets/autocomplete/widget.py:93
msgid "solr"
msgstr ""
